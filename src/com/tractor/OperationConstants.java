package com.tractor;

/**
 * Operation final constants only;
 */
public class OperationConstants {

    public static final int MAP_CONTAINER = 5;
    public static final int MODEL_CONTAINER = 7;
    public static final int OBJECT_CONTAINER = 0;
    public static final int MUSIC_CONTAINER = 0;
}
