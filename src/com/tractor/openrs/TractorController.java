package com.tractor.openrs;

import com.tractor.tools.ModelViewer;
import com.tractor.tools.impl.convsf.ConversionCommand;
import com.tractor.tools.impl.pull.PullCommand;
import com.tractor.windows.terminal.Console;
import com.tractor.windows.terminal.DefaultCompletionSource;
import com.tractor.windows.terminal.InputProcessor;
import com.tractor.openrs.cache.Cache;
import com.tractor.openrs.cache.FileStore;
import com.tractor.openrs.cache.tools.*;
import com.tractor.openrs.cache.type.areas.AreaTypeList;
import com.tractor.openrs.cache.type.enums.EnumTypeList;
import com.tractor.openrs.cache.type.hitbars.HitBarTypeList;
import com.tractor.openrs.cache.type.hitmarks.HitMarkTypeList;
import com.tractor.openrs.cache.type.identkits.IdentkitTypeList;
import com.tractor.openrs.cache.type.invs.InvTypeList;
import com.tractor.openrs.cache.type.items.ItemTypeList;
import com.tractor.openrs.cache.type.npcs.NpcTypeList;
import com.tractor.openrs.cache.type.objects.ObjectTypeList;
import com.tractor.openrs.cache.type.overlays.OverlayTypeList;
import com.tractor.openrs.cache.type.params.ParamTypeList;
import com.tractor.openrs.cache.type.sequences.SequenceTypeList;
import com.tractor.openrs.cache.type.spotanims.SpotAnimTypeList;
import com.tractor.openrs.cache.type.structs.StructTypeList;
import com.tractor.openrs.cache.type.underlays.UnderlayTypeList;
import com.tractor.openrs.cache.type.varbits.VarBitTypeList;
import com.tractor.openrs.cache.type.varclients.VarClientTypeList;
import com.tractor.openrs.cache.type.varclientstrings.VarClientStringTypeList;
import com.tractor.openrs.cache.type.varplayers.VarPlayerTypeList;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.MutableAttributeSet;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * RuneTractor Controller
 *
 * @author trees <>http://rune-server.ee/members/trees</>
 */
public class TractorController {

    private static final SpotAnimTypeList spot = new SpotAnimTypeList();
    private static final EnumTypeList enm = new EnumTypeList();
    private static final IdentkitTypeList ident = new IdentkitTypeList();
    private static final InvTypeList inv = new InvTypeList();
    private static final ItemTypeList item = new ItemTypeList();
    private static final NpcTypeList npc = new NpcTypeList();
    private static final ObjectTypeList obj = new ObjectTypeList();
    private static final OverlayTypeList over = new OverlayTypeList();
    private static final SequenceTypeList seq = new SequenceTypeList();
    private static final UnderlayTypeList under = new UnderlayTypeList();
    private static final VarBitTypeList varbit = new VarBitTypeList();
    private static final VarClientTypeList varc = new VarClientTypeList();
    private static final VarClientStringTypeList varcstr = new VarClientStringTypeList();
    private static final VarPlayerTypeList varp = new VarPlayerTypeList();
    private static final AreaTypeList area = new AreaTypeList();
    private static final ParamTypeList param = new ParamTypeList();
    private static final StructTypeList struct = new StructTypeList();
    private static final HitMarkTypeList hitmark = new HitMarkTypeList();
    private static final HitBarTypeList hitbar = new HitBarTypeList();

    /**
     * System Logger ex:(println)
     */
    public static MutableAttributeSet defaultStyle;
    public static Logger logger = Logger.getLogger(HitBarTypeList.class.getName());
    public static Console console = new Console(Color.LIGHT_GRAY, Color.BLACK,
            new Font(Font.MONOSPACED, Font.LAYOUT_LEFT_TO_RIGHT, 14), "$ ");

    /**
     * Load all the stores
     */
    public static void loadAllStores() {
        try (Cache cache = new Cache(FileStore.open(Constants.CACHE_PATH))) {
            getEnm().initialize(cache);
            getIdent().initialize(cache);
            getNpc().initialize(cache);
            getItem().initialize(cache);
            getSpotAnimType().initialize(cache);
            getObjectType().initialize(cache);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        }
        logger.info("RuneTractor has loaded all types from cache '" + Constants.CACHE_PATH + "'");
    }

    /**
     * Generate Version Table
     *
     * @throws IOException
     */
    public static void generateVersionTable() throws Exception {
        logger.info("Attempting to generate version table...");
        VersionTableGenerator.main(null);
    }

    /**
     * Dump Item Models
     *
     * @throws IOException
     */
    public static void dumpItemModels() throws IOException {
        logger.info("Attempting to dump item models...");
        ItemModelDumper.main(null);
    }

    /**
     * Dump NPC Models
     *
     * @throws IOException
     */
    public static void dumpNPCModels() throws IOException {
        logger.info("Attempting to dump npc models...");
        NpcModelDumper.main(null);
    }

    /**
     * Dump Object Models
     *
     * @throws IOException
     */
    public static void dumpObjectModels() throws IOException {
        logger.info("Attempting to dump object models...");
        ObjectModelDumper.main(null);
    }

    /**
     * Dump Map Images
     *
     * @throws IOException
     */
    public static void dumpMapImages() {
        logger.info("Attempting to dump map images...");
        MapImageDumper.main(null);
    }

    /**
     * Dump Map Files
     *
     * @throws IOException
     */
    public static void dumpMapFiles() throws IOException {
        logger.info("Attempting to dump map files...");
        MapDumper.main(null);
    }

    /**
     * Dump Map Files
     *
     * @throws IOException
     */
    public static void dumpMusicFiles() throws Exception {
        logger.info("Attempting to dump music files...");
        TrackDumper.main(null);
    }

    /**
     * Dump Sprite Files
     *
     * @throws IOException
     */
    public static void dumpSpriteFiles() throws IOException {
        logger.info("Attempting to dump sprite files...");
        SpriteDumper.main(null);
    }

    /**
     * Dump All Cache Types (.txt)
     *
     * @throws IOException
     */
    public static void dumpAllTypeData() {
        logger.info("Attempting to dump typelist data...");
        item.print();
        npc.print();
        obj.print();
        spot.print();
        enm.print();
        ident.print();
    }

    /**
     * Dump Items (.txt)
     */
    public static void printItems() {
        item.print();
    }

    /**
     * Dump NPCs (.txt)
     */
    public static void printNpcs() {
        npc.print();
    }

    /**
     * Dump Anims (.txt)
     */
    public static void printAnims() {
        spot.print();
    }

    /**
     * Dump Objects (.txt)
     */
    public static void printObjects() {
        obj.print();
    }

    public static ItemTypeList getItemType() {
        return item;
    }

    public static NpcTypeList getNpcType() {
        return npc;
    }

    public static SpotAnimTypeList getSpotAnimType() {
        return spot;
    }

    public static ObjectTypeList getObjectType() {
        return obj;
    }

    public static ItemTypeList getItem() {
        return item;
    }

    public static NpcTypeList getNpc() {
        return npc;
    }

    public static EnumTypeList getEnm() {
        return enm;
    }

    public static IdentkitTypeList getIdent() {
        return ident;
    }

    private static final String CONSOLE_NAME = "RuneTractor Terminal";
    private static final String ICON_IMAGE_FILE = "./img/logo.png";
    private static final Color BACKGROUND_COLOR = Color.BLACK;
    private static final Color FOREGROUND_COLOR = Color.BLACK;
    private static final Map<String, InputProcessor> commandMap = new HashMap<String, InputProcessor>(50);

    public static void openTerminalWindow() {

        InputProcessor openModelViewer = new InputProcessor() {
            public void process(String[] args, Console console) {
                ModelViewer.run(args, console);
            }
        };

        InputProcessor clearScreen = new InputProcessor() {
            public void process(String[] args, Console console) {
                console.cls();
            }
        };

        InputProcessor terminateProgram = new InputProcessor() {
            public void process(String[] args, Console console) {
                System.exit(0);
            }
        };

        InputProcessor convSpawn = new InputProcessor() {
            public void process(String[] args, Console console) {
                console.write("Command unstable - Use at own risk");
                ConversionCommand.convert(args, console);
            }
        };

        InputProcessor pullFile = new InputProcessor() {
            public void process(String[] args, Console console) {
                PullCommand.pullFile(args, console);
            }
        };

        InputProcessor echo = new InputProcessor() {
            public void process(String[] args, Console console) {
                console.write(args[1]); // only echos the first word...
            }
        };

        InputProcessor unrecognizedCommand = new InputProcessor() {
            public void process(String[] args, Console console) {
                console.write("Sorry, the command '" + args[0] + "' is unrecognized.");
            }
        };

        commandMap.put("cls", clearScreen);
        commandMap.put("convsf", convSpawn);
        commandMap.put("pull", pullFile);
        commandMap.put("mv", openModelViewer);
        commandMap.put("terminate", terminateProgram);
        commandMap.put("echo", echo);
        commandMap.put("help", unrecognizedCommand);

        JFrame frame = new JFrame(CONSOLE_NAME);
        try {
            frame.setIconImage(ImageIO.read(new File(ICON_IMAGE_FILE)));
        } catch (IOException e) {
            // return
        }
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(677, 343);

        console.setPreferredSize(new Dimension(677, 343));
        console.setCompletionSource(new DefaultCompletionSource("help", "echo", "cls", "close", "exit", "pull", "convsf", "viewer"));
        console.setProcessor(new InputProcessor() {
            public void process(String[] args, Console console) {
                //System.out.println(stream);
                if (args.length > 0 && commandMap.containsKey(args[0].toLowerCase()))
                    commandMap.get(args[0].toLowerCase()).process(args, console);
                else
                    commandMap.get("help").process(args, console);
            }
        });

        frame.add(console);
        frame.addComponentListener(console);
        frame.pack();
        console.setScreenHeight((int) frame.getContentPane().getSize().getHeight());
        frame.setVisible(true);
    }
}
