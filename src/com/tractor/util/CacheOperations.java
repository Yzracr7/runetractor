package com.tractor.util;

import com.tractor.OperationConstants;
import com.tractor.openrs.Constants;
import com.tractor.openrs.TractorController;
import com.tractor.openrs.cache.Cache;
import com.tractor.openrs.cache.Container;
import com.tractor.openrs.cache.FileStore;
import com.tractor.openrs.cache.ReferenceTable;
import com.tractor.openrs.cache.util.XTEAManager;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Operations used by the many commands and buttons across the app
 *
 * @author trees <>http://rune-server.ee/members/trees</>
 */
public class CacheOperations {

    /**
     * Replaces a specific file from a given container
     */
    public static void replaceFile(Cache cache, int file_id, int container_type, File directory) throws IOException {
        //TODO Build packing shit
    }

    /**
     * Does actual file dumping
     *
     * @param cache
     * @param file_id
     * @param container_type
     * @param directory
     * @throws IOException
     */
    private static void dumpFile(Cache cache, int file_id, int container_type, File directory) throws IOException {
        Container container = cache.read(container_type, file_id);
        byte[] bytes = new byte[container.getData().limit()];
        container.getData().get(bytes);
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File(directory, file_id + ".dat")))) {
            dos.write(bytes);
            dos.close();
        }
    }

    /**
     * Pulls a specific map file and landscape of a region
     *
     * @param region
     * @throws IOException
     */
    public static void pullMap(int region) throws IOException {
        File directory = new File(Constants.MAP_PATH + region + "/");
        System.out.println("pulling map :" + directory.getPath());
        if (!directory.exists()) {
            directory.mkdir();
        }

        try (Cache cache = new Cache(FileStore.open(Constants.CACHE_PATH))) {
            int[] keys = XTEAManager.lookupMap(region);
            System.out.println("keys :" + keys);
            int x = (region >> 8);
            int y = (region & 0xFF);

            int map = cache.getFileId(OperationConstants.MAP_CONTAINER, "m" + x + "_" + y);
            int land = cache.getFileId(OperationConstants.MAP_CONTAINER, "l" + x + "_" + y);
            System.out.println("Map Found=" + map + " - Land Found=" + land);
            if (map != -1) {
                Container container = cache.read(OperationConstants.MAP_CONTAINER, map);
                System.out.println("map container=" + container.getVersion());

                byte[] bytes = new byte[container.getData().limit()];
                container.getData().get(bytes);

                File file = new File(Constants.MAP_PATH + region + "/", map + ".dat");
                System.out.println("file...." + file.getPath());

                DataOutputStream dos = new DataOutputStream(new FileOutputStream(file));
                System.out.println("About to write");
                dos.write(bytes);
     //           dos.close();
            }
            System.out.println("here");

            if (land != -1) {
                try {
                    System.out.println("here222");


                    Container container = cache.read(OperationConstants.MAP_CONTAINER, land, keys);
                    System.out.println("container " + container.getVersion());
                    byte[] bytes = new byte[container.getData().limit()];
                    container.getData().get(bytes);
                    System.out.println("LAND");

                    File file = new File(Constants.MAP_PATH + region + "/", land + ".dat");
                    System.out.println("LAND FILE " + file);

                    DataOutputStream dos = new DataOutputStream(new FileOutputStream(file));
                    dos.write(bytes);
                    dos.close();
                } catch (Exception e) {
                    TractorController.logger.info(region + ", " + Arrays.toString(keys));
                }
            }
        }
    }

    /**
     * Pulls a specific model
     *
     * @param model
     * @throws IOException
     */
    public static void pullModel(int model) throws IOException {
        File directory = new File(Constants.MODEL_PATH + "/" + model);

        if (!directory.exists()) {
            directory.mkdirs();
        }

        try (Cache cache = new Cache(FileStore.open(Constants.CACHE_PATH))) {
            ReferenceTable table = cache.getReferenceTable(OperationConstants.MODEL_CONTAINER);

            if (model > table.capacity()) {
                TractorController.logger.info("Model # provided is higher than the model container count");
                return;
            }

            if (table.getEntry(model) != null) {
                dumpFile(cache, model, OperationConstants.MODEL_CONTAINER, directory);
            }

            TractorController.logger.info("Model [" + model + "] successfully pulled");
        }
    }
}