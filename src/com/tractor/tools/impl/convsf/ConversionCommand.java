package com.tractor.tools.impl.convsf;

import com.tractor.openrs.TractorController;
import com.tractor.windows.terminal.Console;


/**
 * Json spawn data conversion tool
 *
 * @author trees <>rune-server.ee/members/trees/</>
 */
public class ConversionCommand {

    /**
     * Used to convert the standard json spawn data we get from dumping with
     * rsbot scripts
     *
     * @param args
     * @param console
     */
    public static void convert(String[] args, Console console) {
        TractorController.logger.info("Attempting to convert json spawn file...");

        switch (args[1]) {
            case "rsmod":
                String json_file_location = args[2];
                String output_file_location = args[3];
                new Converter(new String[]{args[1], json_file_location, output_file_location});
                break;
            default:
                TractorController.logger.info("[Arguments] Args[1]:" + args[1] + " Args[2]:" + args[2] + " ~ Args[3]:" + args[3]);
                break;
        }
    }
}
