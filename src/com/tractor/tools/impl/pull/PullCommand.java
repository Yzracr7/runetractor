package com.tractor.tools.impl.pull;

import com.tractor.openrs.TractorController;
import com.tractor.util.CacheOperations;
import com.tractor.windows.terminal.Console;

import java.io.*;

/**
 * Conversion tool for converting the generic JSON format the npc spawn dumpers create, the
 * results depend on the arg provided
 *
 * @author trees <>rune-server.ee/members/trees/</>
 */
public class PullCommand {

    /**
     * @param args
     * @param console
     */
    public static void pullFile(String[] args, Console console) {
        TractorController.logger.info("Attempting to pull file...");

        int arg_count = args.length;

        switch (args[1]) {
            case "model":
                int model_id = Integer.valueOf(args[2]);
                try {
                    CacheOperations.pullModel(model_id);
                } catch (IOException e) {
                    TractorController.logger.info("Failed to pull model, file not found... Try reloading your cache");
                }
                break;
            case "map":
                int region_id = Integer.valueOf(args[2]);
                try {
                    CacheOperations.pullMap(region_id);
                } catch (IOException e) {
                    TractorController.logger.info("Failed to pull map, region not found... Try reloading your cache");
                }
                break;
            default:
                TractorController.logger.info("[Arguments] Args[1]:" + args[1] + " Args[2]:" + args[2] + " ~ Args[3]:" + args[3]);
                break;
        }
    }

}