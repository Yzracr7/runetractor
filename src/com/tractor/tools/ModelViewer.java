package com.tractor.tools;

import com.tractor.openrs.TractorController;
import com.tractor.windows.terminal.Console;

import java.io.IOException;
import java.io.InputStream;

/**
 * Opens the model viewer
 *
 * @author trees <>rune-server.ee/members/trees/</>
 */
public class ModelViewer {

    /**
     * @param args
     * @param console
     */
    public static void run(String[] args, Console console) {
        try {
            Process proc = Runtime.getRuntime().exec("java -jar data/libs/model_viewer.jar");
            InputStream in = proc.getInputStream();
            InputStream err = proc.getErrorStream();
            TractorController.logger.info("Model viewer has been initiated");
        } catch (IOException e) {
            TractorController.logger.info("An error has occurred loading the model viewer");
        }
    }
}